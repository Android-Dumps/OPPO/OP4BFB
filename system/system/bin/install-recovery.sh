#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:0fb2086dc164c82f6bba5a193c8d94c1006a5ce9; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:594ffec29a56295b7e8ca16e126d2aabbe936d7a EMMC:/dev/block/platform/bootdevice/by-name/recovery 0fb2086dc164c82f6bba5a193c8d94c1006a5ce9 67108864 594ffec29a56295b7e8ca16e126d2aabbe936d7a:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
